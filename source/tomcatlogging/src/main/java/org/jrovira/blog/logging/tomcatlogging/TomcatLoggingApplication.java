package org.jrovira.blog.logging.tomcatlogging;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TomcatLoggingApplication {

	public static void main(String[] args) {
		SpringApplication.run(TomcatLoggingApplication.class, args);
	}
}
