package org.jrovira.blog.logging.tomcatlogging;

import javax.servlet.Filter;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import ch.qos.logback.access.tomcat.LogbackValve;

@Configuration
@Profile("accesslog")
public class AccessLogConfiguration {

	@Bean(name = "TeeFilter")
	public Filter teeFilter() {
	    return new ch.qos.logback.access.servlet.TeeFilter();
	}
	
	@Bean
	public TomcatServletWebServerFactory servletContainer() {
	    TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory();
	    // put logback-access.xml in src/main/resources/conf
	    tomcat.addContextValves(new LogbackValve());
	    return tomcat;
	}
}
